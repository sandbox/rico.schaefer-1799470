mbe4 mobile payment for Drupal Ubercart 2

1. Description
2. Installation
3. Deinstallation
4. Content IDs

1. Description
------------------------------------------------------------
Easy Payment with mbe4 mobile payment method for Ubercart 2

2. Installation
------------------------------------------------------------
2.1. Copy uc_mbe4-folder to sites/all/modules
2.2. Enable module at Site Building -> Modules
2.3. Configure Payment Method at 
     Store -> Settings -> Payment Methods -> mbe4 mobile payment


3. Deinstallation
------------------------------------------------------------
3.1. Deactivate Module at Site Building -> Modules
3.2. Deinstall module at Site Building -> Modules -> Deinstall

4. Content IDs
------------------------------------------------------------
    1: News/Info
    2. Chat/Flirt
    3. Game
    4. Klingelton
    5. Bild/Logo
    6. Videoclip
    7. Musikdatei
    8. Lokalisierung
    9. Voting
    10. Gewinnspiel
    11. Portal Zugang
    12. Software
    13. Dokument
    14. Ticket
    15. Horoskop
    16. Freizeit
    17. Unterwegs
    18. Finanzen
    19. Shopping
    20. E-Mail
    21. Spende


5. Other
------------------------------------------------------------
